﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;


public class followCursor : MonoBehaviour {

    public Vector3 MousePos;
    public Tilemap World;
    public RaycastHit2D rayHit;
    public RaycastHit2D hit;
    public TileBase selectedTile;
    void Start() {
        this.gameObject.GetComponent<SpriteRenderer>().color=Color.white;
    }
    void Update() {
        hit=Physics2D.Raycast(origin: new Vector2Int(Mathf.CeilToInt(MousePos.x), Mathf.FloorToInt(MousePos.y)), direction: Vector2Int.zero);
        MousePos=Camera.main.ScreenToWorldPoint(Input.mousePosition);
        this.transform.position=new Vector3Int(Mathf.CeilToInt(MousePos.x), Mathf.FloorToInt(MousePos.y), 0);
        StartCoroutine(BreakOrPlaceBlock());

    }
    IEnumerator BreakOrPlaceBlock() {
        var worldMap = World.GetComponent<Tilemap>();
        if(Input.GetMouseButtonDown(0)) {
            if(hit.collider!=null&&hit.collider.gameObject.name!="Player") {
                worldMap.SetTile(new Vector3Int(Mathf.CeilToInt(MousePos.x), Mathf.FloorToInt(MousePos.y), 0), null);
            }
        }
        else if(Input.GetMouseButtonDown(1)) {
            if(worldMap.GetTile(new Vector3Int(Mathf.CeilToInt(MousePos.x), Mathf.FloorToInt(MousePos.y), 0))==null) {
                worldMap.SetTile(new Vector3Int(Mathf.CeilToInt(MousePos.x), Mathf.FloorToInt(MousePos.y), 0), selectedTile);
            }
            else {
                StartCoroutine(BuildBlocked());
            }

        }
        yield return new WaitForSeconds(.001f);
    }
    IEnumerator BuildBlocked() {
        this.gameObject.GetComponent<SpriteRenderer>().color=Color.red;
        yield return new WaitForSeconds(0.5f);
        this.gameObject.GetComponent<SpriteRenderer>().color=Color.white;
    }
}
