﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;
public class GenerateTilemapWorld : MonoBehaviour
{
    [Header("Player Settings")]
    public GameObject player;
    public Vector3 playerSpawn;
    bool playerspawned = false;
    [Header("Tree Settings")]
    [Range(5, 25)]
    public int treeGenChance; //Also this variable keeps the maxHeight of all trees. 
    private TileBase selectedTreeTile;
    [Header("Surface Blocks")]
    public TileBase grass;
    public TileBase dirt;
    [Header("Underground Blocks")]
    public TileBase stone;
    [Header("World Settings")]
    public Tilemap decoration_map;
    public Tilemap Water_Map;
    public long Seed;
    public float smoothness;
    [Range(0.05f, 20f)]
    public float smoothnessLerp;
    [Range(60, 250)]
    public int terrainHeight;
    public int seaLevel;
    public int terrainHeightMultiplier;
    public int chunkWidth = 16;
    public int lastPositiveX;
    public int lastNegativeX;
    PerlinNoise noise;
    public GameObject tileFamily;
    Vector3 prevVector3;
    Vector3 vector3Lerp;
    Vector3Int vector3LerpINT;
    void Start() {
        noise=new PerlinNoise(Random.Range(1000000, 10000000));
    }
    void Update() {
        if(Input.GetKeyDown(KeyCode.M)) {
            StartCoroutine(ChunkToRight());
        }
        else if(Input.GetKeyDown(KeyCode.N)) {
            StartCoroutine(ChunkToLeft());
        }
    }
    IEnumerator ChunkToRight() {
        bool nowSeaLevel;
        var block = tileFamily.gameObject.GetComponent<Block>();
        int currentLastPositiveX = lastPositiveX;
        lastPositiveX=lastPositiveX+16;
        for(int j = 0; j<chunkWidth; j++) {
            int h = noise.getNoise(j+currentLastPositiveX, Mathf.RoundToInt((terrainHeightMultiplier*terrainHeight)/smoothness));
            for(int i = 0; i<h+terrainHeight; i++) {
                int randomDirtHeight = Random.Range(15, 19);
                vector3Lerp=Vector3.Lerp(prevVector3, new Vector3(j+currentLastPositiveX, i, 0), smoothnessLerp);
                vector3LerpINT=new Vector3Int(Mathf.RoundToInt(vector3Lerp.x), Mathf.RoundToInt(vector3Lerp.y), 0);
                int actualHeight = h+terrainHeight-1;
                if(i==actualHeight) {
                    if(!playerspawned) {
                        player.transform.position=new Vector3Int(j+currentLastPositiveX, actualHeight+2, 0);
                        playerSpawn=new Vector3Int(j+currentLastPositiveX, actualHeight+2, 0);
                        playerspawned=true;
                    }
                    if(i>terrainHeight+5) {
                        this.gameObject.GetComponent<Tilemap>().SetTile(vector3LerpINT, block.Grass);
                    }
                    else if(i==seaLevel || i!=seaLevel){
                        GenerateWater(Water_Map, j+currentLastPositiveX, i, seaLevel, block.Water);
                    }
                    int treeHeight = Random.Range(5, 150);
                    if(treeHeight<treeGenChance) {
                        GenerateTree(vector3LerpINT.x, vector3LerpINT.y, treeHeight);
                        Debug.Log("Tree Generated!");
                    }
                }
                else if(i<actualHeight&&i>actualHeight-randomDirtHeight) {
                    this.gameObject.GetComponent<Tilemap>().SetTile(vector3LerpINT, block.Dirt);
                }
                else {
                    this.gameObject.GetComponent<Tilemap>().SetTile(vector3LerpINT, block.Stone);
                }
                prevVector3=new Vector3(j+currentLastPositiveX, actualHeight, 0);
            }
            yield return new WaitForSeconds(0.001f);
        }
    }
    IEnumerator ChunkToLeft() {
        var block = tileFamily.gameObject.GetComponent<Block>();
        int currentLastNegativeX = lastNegativeX;
        lastNegativeX=lastNegativeX-16;
        for(int j = 0; j<chunkWidth; j++) {
            int h = noise.getNoise(j-currentLastNegativeX, Mathf.RoundToInt((terrainHeightMultiplier*terrainHeight)/smoothness));
            int actualHeight = h+terrainHeight - 1;
            for(int i = 0; i<actualHeight; i++) {
                int randomDirtHeight = Random.Range(15, 19);
                vector3Lerp=Vector3.Lerp(prevVector3, new Vector3((j-currentLastNegativeX), i, 0), smoothnessLerp);
                vector3LerpINT=new Vector3Int(Mathf.RoundToInt(vector3Lerp.x), Mathf.RoundToInt(vector3Lerp.y), 0);
                if(i==actualHeight) {
                    this.gameObject.GetComponent<Tilemap>().SetTile(vector3LerpINT, grass);
                    int treeHeight = Random.Range(5, 150);
                    if(treeHeight<treeGenChance) {
                        GenerateTree(vector3LerpINT.x, vector3LerpINT.y+1, treeHeight);
                        Debug.Log("Tree Generated!");
                    }
                }
                else if(i<actualHeight&&i>actualHeight-randomDirtHeight) {
                    this.gameObject.GetComponent<Tilemap>().SetTile(vector3LerpINT, dirt);
                }
                else {
                    this.gameObject.GetComponent<Tilemap>().SetTile(vector3LerpINT, stone);
                }
                prevVector3=new Vector3(j+currentLastNegativeX, i, 0);
            }
            yield return new WaitForSeconds(0.001f);
        }
    }
    public void GenerateTree(int x, int y, int treeHeight) {
        var block = tileFamily.gameObject.GetComponent<Block>();
        for(int i = 0; i<=treeHeight; i++) {
            int branchChance = Random.Range(1, 10);
            if(branchChance==1&&i!=treeHeight-1) {
                selectedTreeTile=block.leftBranchBody;
                if(decoration_map.GetTile(new Vector3Int(x-1, y+i, 0))==null) {
                    decoration_map.SetTile(new Vector3Int(x-1, y+i, 0), block.leftBranch);
                }
            }
            else if(branchChance==2&&i!=treeHeight-1) {
                selectedTreeTile=block.rightBranchBody;
                if(decoration_map.GetTile(new Vector3Int(x+1, y+i, 0))==null) {
                    decoration_map.SetTile(new Vector3Int(x+1, y+i, 0), block.rightBranch);
                }
            }

            else if(i==treeHeight-1) {
                var isColliding = decoration_map.GetTile(new Vector3Int(x, y+i+1, 0))==null;
                selectedTreeTile=block.treeBody;
                if(isColliding) {
                    GenerateTopOfTheTree(decoration_map, x, y, i);
                }
            }
            else {
                selectedTreeTile=block.treeBody;
            }
            if(decoration_map.GetTile(new Vector3Int(x, y+i, 0))==null) {
                decoration_map.SetTile(new Vector3Int(x, y+i, 0), selectedTreeTile);
            }
        }
    }
    private void GenerateWater(Tilemap a, int x, int y, int sealevel, TileBase water) {
        for(int i = y; i<=sealevel; i++) {
            a.SetTile(new Vector3Int(x, i, 0), water);
            Debug.Log("Tilemap : " + a +  " - x : " + x + " - y : " + y + " - Sea Level : " + sealevel );
        }
    }
    private void GenerateTopOfTheTree(Tilemap a, int x, int y, int i) {
        var block = tileFamily.gameObject.GetComponent<Block>();
        a.SetTile(new Vector3Int(x-1, y+i+2, 0), block.TreeTop[0]);
        a.SetTile(new Vector3Int(x, y+i+2, 0), block.TreeTop[1]);
        a.SetTile(new Vector3Int(x+1, y+i+2, 0), block.TreeTop[2]);
        a.SetTile(new Vector3Int(x-1, y+i+1, 0), block.TreeTop[3]);
        a.SetTile(new Vector3Int(x, y+i+1, 0), block.TreeTop[4]);
        a.SetTile(new Vector3Int(x+1, y+i+1, 0), block.TreeTop[5]);
    }
}