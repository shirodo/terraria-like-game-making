﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Block : MonoBehaviour
{
    public TileBase Stone;
    public TileBase Grass;
    public TileBase Dirt;
    public TileBase Water;

    [Header("Tree")]
    public TileBase treeBody;
    public TileBase leftBranch;
    public TileBase rightBranch;
    public TileBase leftBranchBody;
    public TileBase rightBranchBody;
    public List<TileBase> TreeTop;

}
