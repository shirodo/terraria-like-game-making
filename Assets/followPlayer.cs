﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followPlayer : MonoBehaviour
{
    [Tooltip("The target object which is generally a player.")]
    public GameObject Player;
    [Range(-1,1)]
    public float offset;
    [Tooltip("Makes the object anchored to the grid.")]
    public bool Wrap;
    void LateUpdate()
    {
        var pos = Player.transform.position;
        if(Wrap) {
            this.transform.position=new Vector3(Mathf.RoundToInt(pos.x)+offset, Mathf.FloorToInt(pos.y), -5);
        }
        else {
            this.transform.position=new Vector3(pos.x+offset, pos.y, -5);
        }
        
    }
}
