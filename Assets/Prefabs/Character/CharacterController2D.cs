﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class CharacterController2D : MonoBehaviour {
    bool isWalking;
    [Range(1f, 3f)]
    public float raycastSize;
    public float speed = 10.0f;
    [Range(1f, 3f)]
    public float runSpeedMultiplier;
    float defaultSpeed;
    public float jumpForce = 20.0f;
    public float airDrag = 0.8f;

    public Rigidbody2D body;
    private SpriteRenderer spriteRenderer;

    public Vector2 currentVelocity;
    private float previousPositionY;
    public LayerMask groundLayer;

    public BoxCollider2D stepAction;
    public GameObject followCursr;
    public GameObject blockFamily;
    bool IsGround;
    // Start is called before the first frame update
    void Start() {
        defaultSpeed=speed;
        var BlockTile = blockFamily.GetComponent<Block>();
        var flwCursor = followCursr.GetComponent<followCursor>();
        flwCursor.selectedTile=BlockTile.Stone;
        body=GetComponent<Rigidbody2D>();
        spriteRenderer=GetComponent<SpriteRenderer>();

    }
    public bool IsGrounded() {
        Vector2 position = transform.position;
        Vector2 direction = Vector2.down;
        float distance = raycastSize;

        RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, groundLayer);
        if(hit.collider!=null) {
            return true;
        }

        return false;
    }
    void setSpeed() {
        if(Input.GetKeyDown(KeyCode.LeftShift)) {
            speed=defaultSpeed*runSpeedMultiplier;
        }
        else if(Input.GetKeyUp(KeyCode.LeftShift)) {
            speed=defaultSpeed;
        }
    }
    private void FixedUpdate() {
        setSpeed();
        var BlockTile = blockFamily.GetComponent<Block>();

        var flwCursor = followCursr.GetComponent<followCursor>();
        if(Input.GetKeyDown(KeyCode.Alpha1)) { flwCursor.selectedTile=BlockTile.Stone; }
        else if(Input.GetKeyDown(KeyCode.Alpha2)) { flwCursor.selectedTile=BlockTile.Grass; }
        else if(Input.GetKeyDown(KeyCode.Alpha3)) { flwCursor.selectedTile=BlockTile.Dirt; }
        Move();
        previousPositionY=transform.position.y;
        if(Input.GetKey(KeyCode.A)||Input.GetKey(KeyCode.D)) {
            isWalking=true;
        }
        if(Input.GetKey(KeyCode.W)) {
            Jump();
        }
        else {
            isWalking=false;
        }
    }
    public void OnTriggerStay2D(Collider2D collision) {
        if(collision.gameObject.name=="World"&&isWalking) {
            Jump();
        }
    }
    public void OnTriggerExit2D(Collider2D collider) {
        Debug.Log("Light of the hope!");
    }
    void Jump() {
        if(IsGrounded()) {
            body.velocity=new Vector2(0, jumpForce);
        }
    }
    private void Move() {

        float velocity = Input.GetAxis("Horizontal")*speed;
        bool isJumping = Input.GetKey(KeyCode.Space);

        velocity*=airDrag;

        body.velocity=Vector2.SmoothDamp(body.velocity, new Vector2(velocity, body.velocity.y), ref currentVelocity, 0.02f);

        if(velocity<0) spriteRenderer.flipX=true;
        else if(velocity>0)
            spriteRenderer.flipX=false;
    }
}